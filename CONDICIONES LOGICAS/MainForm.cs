﻿/*
 * Created by SharpDevelop.
 * User: BLACKHAT
 * Date: 04/11/2016
 * Time: 23:45
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
namespace CONDICIONES_LOGICAS
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public float limiteintermedio; 
		private calibracion calib;
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		
		}
		
		
		
	
		
		void Button1Click(object sender, EventArgs e)
		{
		//CONDICIONES TRABAJANDO CON NUMEROS ENTEROS	
		int numval = Convert.ToInt32(textBox1.Text);
		if (numval == 34) {
			panel1.BackColor = Color.Red;
			panel2.BackColor = Color.Gray;
		}
		
		if (numval==35) {
			panel2.BackColor = Color.Peru;
			panel1.BackColor = Color.Gray;
		}
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			//limiteintermedio = Convert.ToSingle(textBox5.Text);
			float floatval = Convert.ToSingle(textBox2.Text);
			float limitealto = Convert.ToSingle(textBox3.Text);
			float limitebajo = Convert.ToSingle(textBox4.Text);
			if (floatval < limitealto && floatval > limitebajo) {
				panel3.BackColor = Color.Gold;
			}
			//if (floatval < limitebajo) {
			//	panel3.BackColor = Color.Gray;
			//}
			//if (limiteintermedio > 100.6) {
			//	panel3.BackColor =Color.Green;
			//}
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			textBox3.Text = System.IO.File.ReadAllText("limites.txt").Trim();
			calib = new calibracion();
			calib.Show();
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			//calibracion calib = new calibracion();
			//calib.Show();
		}
		
		void CalibextClick(object sender, EventArgs e)
		{
			errorProvider1.Clear();
			decimal valortex3 = 0;
			if (!decimal.TryParse(textBox3.Text ,out valortex3)) {
				errorProvider1.SetError(textBox3,"Formato invalido");
				return;
			}
			System.IO.File.WriteAllText("limites.txt" , textBox3.Text);
		}
		
		public void Append(string type , string text)
		{
			bool debug = false;
			richTextBox1.Focus();
			switch (type) {
				case  "error":
					richTextBox1.SelectionColor = Color.Red;
					break;
				case "sucess":
					richTextBox1.SelectionColor = Color.LimeGreen;
					break;
				case "info":
					richTextBox1.SelectionColor = Color.Blue;
					break;
				case "debug":
					richTextBox1.SelectionColor = Color.DarkGray;
					break;
					debug = true;
					
				
					
					
			}
			if (!debug || cbdebug.Checked ) {
				string s = DateTime.Now.ToString("hh:mm:ss:ff") + text;
				richTextBox1.AppendText(s + Environment.NewLine);
				using (StreamWriter sw =File.AppendText("log.txt"))
				{
					sw.WriteLine(s);
				}
				
			}
		}
		
		void Button4Click(object sender, EventArgs e)
		{
			label9.Text=calib.textBox2.Text;
			textBox6.Text=calib.textBox2.Text;
			Append("error" , "el mensaje se imprime correcto");
			
			if (cb1.Checked == true) {
				Append("info",	"SE HA ACTIVADO LA CONDICION 1 DEL CHEKEDBOX1");
		}
			if (cb2.Checked == true) {
				Append("info","SE HA ACTIVADO LA CONDICION 2 DEL CHECKEDBOX 2");
			}
			if (cb3.Checked == true) {
				Append("info","SE HA ACTIVADO LA CONDICION 3 DEL CHEKEDBOX 3");
			}
			if (cb4.Checked == true) {
				Append("info" ,"SE HA ACTIVADO LA CONDICION 4 DEL CHECKEDBOX 4");
			}
			if (cb5.Checked == true) {
				Append("info", "SE HA ACTIVADO LA CONDICION 5 DEL CHECKEDBOX 5");
			}
			if (cb6.Checked==true) {
				Append("info", "SE HA ACTIVADO LA CONDICION 6 DEL CHEKEDBOX 6");
			}
			if (cb7.Checked==true) {
				Append("info","SEA HA ACTIVADO LA CONDICION 7 DEL CHECKEDBOX 7");
			}
		}
		
	
		
		void CLOSEAPPClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void Button5Click(object sender, EventArgs e)
		{
			secuencia();
		}
				
		public void secuencia()
		{
			bool complete = false;
			do
			{
				lblpaso1.Visible=true;
			}
			while(complete);
		}
		
		public void metodosecuencial1()
		{
		lblpaso1.Visible = true;
		}
		
		public void metodosecuencial2()
		{
		lblpaso2.Visible = true;
		}
		
		public void metodosecuencial3()
		{
		lblpaso3.Visible = true;
		}
		
		public void metodosecuencial4()
		{
			lblpaso4.Visible = true;
		}
		
		public void metodosecuencial5()
		{
		lblpaso5.Visible =true;
		}
		
		public void metodosecuencial6()
		{
			lblpaso6.Visible = true;
		}
	}
}
