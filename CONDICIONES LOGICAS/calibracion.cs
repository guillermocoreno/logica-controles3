﻿/*
 * Created by SharpDevelop.
 * User: BLACKHAT
 * Date: 05/11/2016
 * Time: 20:54
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace CONDICIONES_LOGICAS
{
	/// <summary>
	/// Description of calibracion.
	/// </summary>
	public partial class calibracion : Form
	{
		public calibracion()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		void SalvarClick(object sender, EventArgs e)
		{
			errorProvider1.Clear();
			decimal valor=0;
			if (!decimal.TryParse(textBox1.Text , out valor)) 
			{
				errorProvider1.SetError(textBox1,"Formato invalido");
				return;
			}
			System.IO.File.WriteAllText("limites.txt",textBox1.Text);
		}
		
		void calibload(object sender, EventArgs e)
		{
			textBox1.Text =System.IO.File.ReadAllText("limites.txt").Trim();
		}
	}
}
