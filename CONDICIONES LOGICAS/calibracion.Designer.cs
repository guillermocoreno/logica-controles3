﻿/*
 * Created by SharpDevelop.
 * User: BLACKHAT
 * Date: 05/11/2016
 * Time: 20:54
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CONDICIONES_LOGICAS
{
	partial class calibracion
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.salvar = new System.Windows.Forms.Button();
			this.cancelar = new System.Windows.Forms.Button();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.textBox2 = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(15, 26);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(66, 27);
			this.label1.TabIndex = 0;
			this.label1.Text = "limite";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(87, 28);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 20);
			this.textBox1.TabIndex = 1;
			// 
			// salvar
			// 
			this.salvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.salvar.Location = new System.Drawing.Point(216, 26);
			this.salvar.Name = "salvar";
			this.salvar.Size = new System.Drawing.Size(122, 27);
			this.salvar.TabIndex = 2;
			this.salvar.Text = "salvar";
			this.salvar.UseVisualStyleBackColor = true;
			this.salvar.Click += new System.EventHandler(this.SalvarClick);
			// 
			// cancelar
			// 
			this.cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cancelar.Location = new System.Drawing.Point(358, 26);
			this.cancelar.Name = "cancelar";
			this.cancelar.Size = new System.Drawing.Size(137, 27);
			this.cancelar.TabIndex = 3;
			this.cancelar.Text = "cancelar";
			this.cancelar.UseVisualStyleBackColor = true;
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(100, 95);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(100, 20);
			this.textBox2.TabIndex = 4;
			// 
			// calibracion
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(507, 163);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.cancelar);
			this.Controls.Add(this.salvar);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label1);
			this.Name = "calibracion";
			this.Text = "calibracion";
			this.Load += new System.EventHandler(this.calibload);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		public System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.Button cancelar;
		private System.Windows.Forms.Button salvar;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
	}
}
