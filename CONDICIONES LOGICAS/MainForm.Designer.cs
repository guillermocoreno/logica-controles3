﻿/*
 * Created by SharpDevelop.
 * User: BLACKHAT
 * Date: 04/11/2016
 * Time: 23:45
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CONDICIONES_LOGICAS
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.calibext = new System.Windows.Forms.Button();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.cbdebug = new System.Windows.Forms.CheckBox();
			this.cb1 = new System.Windows.Forms.CheckBox();
			this.cb2 = new System.Windows.Forms.CheckBox();
			this.cb3 = new System.Windows.Forms.CheckBox();
			this.cb4 = new System.Windows.Forms.CheckBox();
			this.cb5 = new System.Windows.Forms.CheckBox();
			this.cb6 = new System.Windows.Forms.CheckBox();
			this.cb7 = new System.Windows.Forms.CheckBox();
			this.button4 = new System.Windows.Forms.Button();
			this.CLOSEAPP = new System.Windows.Forms.Button();
			this.panel5 = new System.Windows.Forms.Panel();
			this.label9 = new System.Windows.Forms.Label();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.button5 = new System.Windows.Forms.Button();
			this.lblpaso1 = new System.Windows.Forms.Label();
			this.lblpaso2 = new System.Windows.Forms.Label();
			this.lblpaso3 = new System.Windows.Forms.Label();
			this.lblpaso4 = new System.Windows.Forms.Label();
			this.lblpaso5 = new System.Windows.Forms.Label();
			this.lblpaso6 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.panel5.SuspendLayout();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(43, 44);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(128, 20);
			this.textBox1.TabIndex = 0;
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Location = new System.Drawing.Point(27, 119);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(64, 53);
			this.panel1.TabIndex = 1;
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel2.Location = new System.Drawing.Point(152, 118);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(71, 54);
			this.panel2.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(15, 76);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(122, 40);
			this.label1.TabIndex = 3;
			this.label1.Text = "CUANDO EL VALOR EQUIVALE A 34";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(152, 76);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(101, 27);
			this.label2.TabIndex = 4;
			this.label2.Text = "CUANDO EL VALOR EQUIVALE A 35";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(224, 21);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 47);
			this.button1.TabIndex = 5;
			this.button1.Text = "VERIFICAR CONDICION ENTERA";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(3, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(236, 23);
			this.label3.TabIndex = 6;
			this.label3.Text = "CONDICIONES TRABAJANDO NUMEROS ENTEROS";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(0, 198);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(267, 30);
			this.label4.TabIndex = 7;
			this.label4.Text = "CONDICIONES TRABAJANDO NUMEROS FLOTANTES";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(12, 231);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(100, 20);
			this.textBox2.TabIndex = 8;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(12, 266);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(122, 36);
			this.label5.TabIndex = 9;
			this.label5.Text = "CUANDO EL  VALOR EQUIVALE A 13.5";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(135, 266);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(118, 36);
			this.label6.TabIndex = 10;
			this.label6.Text = "CUANDO EL VALOR EQUIVALE A 10.5";
			// 
			// panel3
			// 
			this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel3.Location = new System.Drawing.Point(27, 309);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(64, 60);
			this.panel3.TabIndex = 11;
			// 
			// panel4
			// 
			this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel4.Location = new System.Drawing.Point(152, 305);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(71, 64);
			this.panel4.TabIndex = 12;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(216, 198);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(83, 57);
			this.button2.TabIndex = 13;
			this.button2.Text = "VERIFICAR CONDICION FLOTANTE";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(248, 309);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(73, 20);
			this.textBox3.TabIndex = 14;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(248, 364);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(73, 20);
			this.textBox4.TabIndex = 15;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(251, 279);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(79, 23);
			this.label7.TabIndex = 16;
			this.label7.Text = "LIMITE ALTO";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(248, 335);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(75, 23);
			this.label8.TabIndex = 17;
			this.label8.Text = "LIMITE BAJO";
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(257, 167);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(66, 20);
			this.textBox5.TabIndex = 18;
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(340, 88);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 19;
			this.button3.Text = "calibracion";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// calibext
			// 
			this.calibext.Location = new System.Drawing.Point(327, 12);
			this.calibext.Name = "calibext";
			this.calibext.Size = new System.Drawing.Size(88, 35);
			this.calibext.TabIndex = 20;
			this.calibext.Text = "CALIBRACION EXTERIOR";
			this.calibext.UseVisualStyleBackColor = true;
			this.calibext.Click += new System.EventHandler(this.CalibextClick);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// richTextBox1
			// 
			this.richTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.richTextBox1.Location = new System.Drawing.Point(18, 241);
			this.richTextBox1.Margin = new System.Windows.Forms.Padding(6);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(490, 88);
			this.richTextBox1.TabIndex = 12;
			this.richTextBox1.Text = "";
			// 
			// cbdebug
			// 
			this.cbdebug.Location = new System.Drawing.Point(74, 19);
			this.cbdebug.Name = "cbdebug";
			this.cbdebug.Size = new System.Drawing.Size(104, 24);
			this.cbdebug.TabIndex = 22;
			this.cbdebug.Text = "debug";
			this.cbdebug.UseVisualStyleBackColor = true;
			// 
			// cb1
			// 
			this.cb1.Location = new System.Drawing.Point(138, 47);
			this.cb1.Name = "cb1";
			this.cb1.Size = new System.Drawing.Size(104, 24);
			this.cb1.TabIndex = 23;
			this.cb1.Text = "condicion 1";
			this.cb1.UseVisualStyleBackColor = true;
			// 
			// cb2
			// 
			this.cb2.Location = new System.Drawing.Point(137, 74);
			this.cb2.Name = "cb2";
			this.cb2.Size = new System.Drawing.Size(104, 24);
			this.cb2.TabIndex = 24;
			this.cb2.Text = "condicion 2";
			this.cb2.UseVisualStyleBackColor = true;
			// 
			// cb3
			// 
			this.cb3.Location = new System.Drawing.Point(137, 100);
			this.cb3.Name = "cb3";
			this.cb3.Size = new System.Drawing.Size(104, 24);
			this.cb3.TabIndex = 25;
			this.cb3.Text = "condicion 3";
			this.cb3.UseVisualStyleBackColor = true;
			// 
			// cb4
			// 
			this.cb4.Location = new System.Drawing.Point(137, 124);
			this.cb4.Name = "cb4";
			this.cb4.Size = new System.Drawing.Size(104, 24);
			this.cb4.TabIndex = 26;
			this.cb4.Text = "condicion 4";
			this.cb4.UseVisualStyleBackColor = true;
			// 
			// cb5
			// 
			this.cb5.Location = new System.Drawing.Point(278, 47);
			this.cb5.Name = "cb5";
			this.cb5.Size = new System.Drawing.Size(104, 24);
			this.cb5.TabIndex = 27;
			this.cb5.Text = "condicion 5";
			this.cb5.UseVisualStyleBackColor = true;
			// 
			// cb6
			// 
			this.cb6.Location = new System.Drawing.Point(278, 69);
			this.cb6.Name = "cb6";
			this.cb6.Size = new System.Drawing.Size(104, 24);
			this.cb6.TabIndex = 28;
			this.cb6.Text = "condicion 6";
			this.cb6.UseVisualStyleBackColor = true;
			// 
			// cb7
			// 
			this.cb7.Location = new System.Drawing.Point(278, 92);
			this.cb7.Name = "cb7";
			this.cb7.Size = new System.Drawing.Size(104, 24);
			this.cb7.TabIndex = 29;
			this.cb7.Text = "condicion 7";
			this.cb7.UseVisualStyleBackColor = true;
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(235, 19);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(158, 23);
			this.button4.TabIndex = 30;
			this.button4.Text = "CHECAR CONDICIONES ";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.Button4Click);
			// 
			// CLOSEAPP
			// 
			this.CLOSEAPP.Location = new System.Drawing.Point(832, 41);
			this.CLOSEAPP.Name = "CLOSEAPP";
			this.CLOSEAPP.Size = new System.Drawing.Size(75, 23);
			this.CLOSEAPP.TabIndex = 31;
			this.CLOSEAPP.Text = "CERRAR";
			this.CLOSEAPP.UseVisualStyleBackColor = true;
			this.CLOSEAPP.Click += new System.EventHandler(this.CLOSEAPPClick);
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.richTextBox1);
			this.panel5.Controls.Add(this.cbdebug);
			this.panel5.Controls.Add(this.button4);
			this.panel5.Controls.Add(this.cb7);
			this.panel5.Controls.Add(this.cb1);
			this.panel5.Controls.Add(this.cb6);
			this.panel5.Controls.Add(this.cb2);
			this.panel5.Controls.Add(this.cb5);
			this.panel5.Controls.Add(this.cb3);
			this.panel5.Controls.Add(this.cb4);
			this.panel5.Location = new System.Drawing.Point(410, 154);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(528, 358);
			this.panel5.TabIndex = 32;
			// 
			// label9
			// 
			this.label9.BackColor = System.Drawing.SystemColors.ControlText;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.Color.Red;
			this.label9.Location = new System.Drawing.Point(258, 455);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(100, 23);
			this.label9.TabIndex = 33;
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(258, 432);
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new System.Drawing.Size(100, 20);
			this.textBox6.TabIndex = 34;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(61, 413);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(141, 51);
			this.button5.TabIndex = 35;
			this.button5.Text = "SECUENCIA";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.Button5Click);
			// 
			// lblpaso1
			// 
			this.lblpaso1.Location = new System.Drawing.Point(447, 102);
			this.lblpaso1.Name = "lblpaso1";
			this.lblpaso1.Size = new System.Drawing.Size(68, 34);
			this.lblpaso1.TabIndex = 36;
			this.lblpaso1.Text = "paso1";
			this.lblpaso1.Visible = false;
			// 
			// lblpaso2
			// 
			this.lblpaso2.Location = new System.Drawing.Point(510, 102);
			this.lblpaso2.Name = "lblpaso2";
			this.lblpaso2.Size = new System.Drawing.Size(68, 34);
			this.lblpaso2.TabIndex = 37;
			this.lblpaso2.Text = "paso2";
			this.lblpaso2.Visible = false;
			// 
			// lblpaso3
			// 
			this.lblpaso3.Location = new System.Drawing.Point(571, 102);
			this.lblpaso3.Name = "lblpaso3";
			this.lblpaso3.Size = new System.Drawing.Size(68, 34);
			this.lblpaso3.TabIndex = 38;
			this.lblpaso3.Text = "paso3";
			this.lblpaso3.Visible = false;
			// 
			// lblpaso4
			// 
			this.lblpaso4.Location = new System.Drawing.Point(645, 102);
			this.lblpaso4.Name = "lblpaso4";
			this.lblpaso4.Size = new System.Drawing.Size(68, 34);
			this.lblpaso4.TabIndex = 39;
			this.lblpaso4.Text = "paso4";
			this.lblpaso4.Visible = false;
			// 
			// lblpaso5
			// 
			this.lblpaso5.Location = new System.Drawing.Point(706, 102);
			this.lblpaso5.Name = "lblpaso5";
			this.lblpaso5.Size = new System.Drawing.Size(68, 34);
			this.lblpaso5.TabIndex = 40;
			this.lblpaso5.Text = "paso5";
			this.lblpaso5.Visible = false;
			// 
			// lblpaso6
			// 
			this.lblpaso6.Location = new System.Drawing.Point(761, 102);
			this.lblpaso6.Name = "lblpaso6";
			this.lblpaso6.Size = new System.Drawing.Size(68, 34);
			this.lblpaso6.TabIndex = 41;
			this.lblpaso6.Text = "paso6";
			this.lblpaso6.Visible = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(954, 556);
			this.Controls.Add(this.lblpaso6);
			this.Controls.Add(this.lblpaso5);
			this.Controls.Add(this.lblpaso4);
			this.Controls.Add(this.lblpaso3);
			this.Controls.Add(this.lblpaso2);
			this.Controls.Add(this.lblpaso1);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.textBox6);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.panel5);
			this.Controls.Add(this.CLOSEAPP);
			this.Controls.Add(this.calibext);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.textBox5);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.textBox4);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.textBox1);
			this.Name = "MainForm";
			this.Text = "CONDICIONES LOGICAS";
			this.Load += new System.EventHandler(this.MainFormLoad);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.panel5.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.Label lblpaso1;
		private System.Windows.Forms.Label lblpaso2;
		private System.Windows.Forms.Label lblpaso3;
		private System.Windows.Forms.Label lblpaso4;
		private System.Windows.Forms.Label lblpaso5;
		private System.Windows.Forms.Label lblpaso6;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Button CLOSEAPP;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.CheckBox cb1;
		private System.Windows.Forms.CheckBox cb2;
		private System.Windows.Forms.CheckBox cb3;
		private System.Windows.Forms.CheckBox cb4;
		private System.Windows.Forms.CheckBox cb5;
		private System.Windows.Forms.CheckBox cb6;
		private System.Windows.Forms.CheckBox cb7;
		private System.Windows.Forms.CheckBox cbdebug;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.Button calibext;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox textBox1;
	}
}
